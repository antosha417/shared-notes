import Dependencies._

ThisBuild / scalaVersion     := "2.13.1"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

val catsEffectVersion     = "2.1.3"
val circeVersion          = "0.13.0"
val http4sVersion         = "0.21.3"
val silencerVersion       = "1.6.0"
val sttpVersion           = "2.0.9"
val zioVersion            = "1.0.0-RC18-2"
val zioInteropCatsVersion = "2.0.0.0-RC12"
val calibanVersion        = "0.7.6"
val doobieVersion         = "0.8.8"
val pureconfigVersion     = "0.12.3"

lazy val root = (project in file("."))
  .settings(
    name := "shared-notes",
    libraryDependencies ++= Seq(
      scalaTest % Test,
      "com.github.ghostdogpr" %% "caliban"              % calibanVersion,
      "com.github.ghostdogpr" %% "caliban-http4s"       % calibanVersion,
      "dev.zio"               %% "zio"                  % zioVersion,
      "dev.zio"               %% "zio-streams"          % zioVersion,
      "dev.zio"               %% "zio-interop-cats"     % zioInteropCatsVersion,
      "org.typelevel"         %% "cats-effect"          % catsEffectVersion,
      "org.http4s"            %% "http4s-dsl"           % http4sVersion,
      "org.http4s"            %% "http4s-circe"         % http4sVersion,
      "org.http4s"            %% "http4s-blaze-server"  % http4sVersion,
      "io.circe"              %% "circe-parser"         % circeVersion,
      "org.tpolecat"          %% "doobie-core"          % doobieVersion,
      "org.tpolecat"          %% "doobie-h2"            % doobieVersion,
      "com.github.pureconfig" %% "pureconfig"           % pureconfigVersion,
      compilerPlugin(
        ("org.typelevel" %% "kind-projector" % "0.11.0")
          .cross(CrossVersion.full)
      )
    )
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
