package sharednotes

import doobie.implicits._
import doobie.util.query.Query0
import doobie.util.update.Update0
import sharednotes.Data._
import sharednotes.db.DB
import zio.interop.catz._
import zio.stream.ZStream
import zio.{Has, Queue, Ref, _}

object Service {

  type NotesService = Has[Service]

  trait Service {
    def getOperations(noteId: String): Task[List[Operation]]

    def addOperation(op: Operation): Task[Boolean]

    def addedOperations(noteId: String): ZStream[Any, Nothing, Operation]
  }

  def getOperations(noteId: String): RIO[NotesService, List[Operation]] =
    URIO.accessM(_.get.getOperations(noteId))

  def addOperation(op: Operation): RIO[NotesService, Boolean] =
    URIO.accessM(_.get.addOperation(op))

  def addedOperations(
      noteId: String
  ): ZStream[NotesService, Nothing, Operation] =
    ZStream.accessStream(_.get.addedOperations(noteId))

  def live: ZLayer[DB, Nothing, NotesService] =
    ZLayer.fromEffect {
      for {
        tnx <- ZIO.access[DB](r => r.get.transactor)
        subscribers <- Ref.make(Map.empty[String, List[Queue[Operation]]])
      } yield new Service {

        def getOperations(noteId: String): Task[List[Operation]] = {
          SQL
            .getOperations(noteId)
            .stream
            .map(t =>
              Operation(
                OperationId(t._1, t._2),
                OperationId(t._3, t._4),
                t._5,
                t._6,
                t._7
              )
            )
            .compile
            .toList
            .transact(tnx)
        }

        def addOperation(op: Operation): Task[Boolean] = {
          SQL
            .put(op)
            .run
            .transact(tnx)
            .fold(_ => false, _ => true)
            .tap(added => UIO.when(added)(notifySubscribers(op)))
        }

        def addedOperations(noteId: String): ZStream[Any, Nothing, Operation] =
          ZStream.unwrap {
            for {
              queue <- Queue.unbounded[Operation]
              _ <- subscribers.update(m =>
                m + (noteId -> (queue :: m.getOrElse(noteId, List())))
              )
            } yield ZStream
              .fromQueue(queue)
              .ensuring(deleteSubscription(noteId, queue) *> queue.shutdown)
          }

        def deleteSubscription(
            noteId: String,
            queue: Queue[Operation]
        ): UIO[Unit] =
          subscribers.update(m => {
            val aliveSubscribers =
              m.getOrElse(noteId, List()).filterNot(_ == queue)
            if (aliveSubscribers.size > 0) {
              m + (noteId -> aliveSubscribers)
            } else {
              m - noteId
            }
          })

        def notifySubscribers(op: Operation): UIO[Unit] =
          for {
            aliveSubscribers <- subscribers.get
            _ <- aliveSubscribers.get(op.noteId) match {
              case None => UIO.succeed(List())
              case Some(l) =>
                ZIO.collectAll(l.map(queue => {
                  queue.offer(op).catchSomeCause {
                    case cause if cause.interrupted =>
                      deleteSubscription(op.noteId, queue).as(false)
                  }
                }))
            }
          } yield ()
      }
    }

  object SQL {
    def getOperations(
        noteId: String
    ): Query0[(Int, String, Int, String, String, Boolean, String)] = sql"""
      SELECT id, uuid, parentId, parentUUID, keystroke, delete, noteId 
      FROM OPERATION
      WHERE noteId = ${noteId}
      ORDER BY id ASC;
    """.query

    def put(op: Operation): Update0 = sql"""
      INSERT INTO OPERATION (id, uuid, parentId, parentUUID, keystroke, delete, noteId) VALUES (${op.id.id}, ${op.id.uuid}, ${op.parentId.id}, ${op.parentId.uuid}, ${op.keystroke}, ${op.delete}, ${op.noteId});
    """.update
  }
}
