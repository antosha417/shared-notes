package sharednotes
import sharednotes.Data._
import caliban.{ Http4sAdapter }
import sharednotes.{ Api, Service }
import cats.data.Kleisli
import cats.effect.Blocker
import org.http4s.StaticFile
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.CORS
import zio._
import zio.blocking.Blocking
import zio.console.putStrLn
import zio.interop.catz._
import sharednotes.db.DBLayer
import sharednotes.configuration.Configuration

object SharedNotes extends CatsApp {

  type ExampleTask[A] = RIO[ZEnv, A]

  val SharedNotesLayer = DBLayer >>> Service.live

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
      SharedNotesLayer
      .memoize
      .use(layer =>
        for {
          api <- configuration.apiConfig.provideCustomLayer(Configuration.live)
          blocker     <- ZIO.access[Blocking](_.get.blockingExecutor.asEC).map(Blocker.liftExecutionContext)
          interpreter <- Api.api.interpreter.map(_.provideCustomLayer(layer))
          _ <- BlazeServerBuilder[ExampleTask]
                .bindHttp(api.port, api.host)
                .withHttpApp(
                  Router[ExampleTask](
                    api.httpEndpoint     -> CORS(Http4sAdapter.makeHttpService(interpreter)),
                    api.wsEndpoint       -> CORS(Http4sAdapter.makeWebSocketService(interpreter)),
                    api.graphiqlEndpoint -> Kleisli.liftF(StaticFile.fromResource("/graphiql.html", blocker, None))
                  ).orNotFound
                )
                .resource
                .toManaged
                .useForever
        } yield 0
      )
      .catchAll(err => putStrLn(err.toString).as(1))
}
