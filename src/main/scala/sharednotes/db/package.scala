package sharednotes

import zio.Has
import sharednotes.configuration.Configuration
import zio.blocking.Blocking

package object db {
  type DB = Has[DB.Service]

  val DBLayer = (Configuration.live ++ Blocking.live) >>> DB.live
}
