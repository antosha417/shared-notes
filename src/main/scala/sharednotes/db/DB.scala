package sharednotes.db


import cats.effect.Blocker
import doobie.h2.H2Transactor
import doobie.util.transactor.Transactor
import sharednotes.configuration.DbConfig
import sharednotes.configuration
import zio._
import zio.blocking.{Blocking, blocking}
import zio.interop.catz._
import sharednotes.configuration.Configuration

object DB {

  // TODO use layer with transoctor instead of this service
  case class Service(transactor: Transactor[Task])

  val live: RLayer[Has[DbConfig] with Blocking, DB] =
    ZLayer.fromManaged(
      for {
        liveEC  <- ZIO.descriptor.map(_.executor.asEC).toManaged_
        blockEC <- blocking(ZIO.descriptor.map(_.executor.asEC)).toManaged_
        conf  <- configuration.dbConfig.toManaged_
        trans <- H2Transactor
                  .newH2Transactor[Task](
                    conf.url,
                    conf.user,
                    conf.password,
                    liveEC,
                    Blocker.liftExecutionContext(blockEC)
                  )
                  .toManagedZIO
      } yield Service(trans)
    )
}
