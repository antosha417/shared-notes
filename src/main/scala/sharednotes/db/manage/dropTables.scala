package sharednotes.db.manage

import cats.effect.Blocker
import zio._
import doobie.{Query0, Update0}
import scala.concurrent.ExecutionContext
import zio.blocking.Blocking
import zio.interop.catz._
import doobie.util.transactor.Transactor
import doobie.implicits._
import zio.blocking.Blocking
import sharednotes.configuration.DbConfig
import sharednotes.Data.{ Operation, OperationId }


object DropTables extends App {
  def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] = {
    runSQL(dropOperationTable)
  }

  def dropOperationTable(tnx: Transactor[Task]): Task[Int] = 
    sql"""
      DROP TABLE IF EXISTS OPERATION;
    """
      .update
      .run
      .transact(tnx)
}
