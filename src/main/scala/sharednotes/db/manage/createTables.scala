package sharednotes.db.manage

import zio._
import zio.interop.catz._
import doobie.util.transactor.Transactor
import doobie.implicits._
import sharednotes.configuration.DbConfig
import sharednotes.db.DB

object CreateTables extends App {
  def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] = {
    runSQL(createOperationTable)
  }

  def createOperationTable(tnx: Transactor[Task]): Task[Int] =
    sql"""
      CREATE TABLE IF NOT EXISTS OPERATION (
        id          INT,
        uuid        VARCHAR(36),
        parentId    INT,
        parentUUID  VARCHAR(36),
        keystroke   VARCHAR(100),
        delete      BOOLEAN,
        noteId      VARCHAR(100)
      );
    """
      .update.run
      .transact(tnx)
}
