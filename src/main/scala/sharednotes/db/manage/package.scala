package sharednotes.db

import doobie.util.transactor.Transactor
import sharednotes.db.DBLayer
import zio._
import zio.console._

package object manage {
  def runSQL(
      sql: Transactor[Task] => Task[Int]
  ): ZIO[zio.ZEnv, Nothing, Int] = {
    (ZIO.access[DB](r => r.get.transactor) >>= sql)
      .provideCustomLayer(DBLayer)
      .foldM(e => putStrLn(e.toString) as 1, x => putStrLn(s"success ${x}") as x)
  }
}
