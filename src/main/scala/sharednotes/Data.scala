package sharednotes

import sharednotes.Data._

object Data {

  case class OperationId(
      id: Int,
      uuid: String
  )

  // TODO think of better names for id and parentId
  case class Operation(
      id: OperationId,
      parentId: OperationId,
      keystroke: String,
      delete: Boolean,
      noteId: String,
  )

  case class OperationsArgs(noteId: String)
}
