package sharednotes

import scala.language.postfixOps
import sharednotes.Data._
import caliban.{ GraphQL, RootResolver }
import sharednotes.{ Service }
import sharednotes.Service.NotesService
import caliban.GraphQL.graphQL
import caliban.schema.Annotations.{ GQLDeprecated, GQLDescription }
import caliban.schema.GenericSchema
import caliban.wrappers.ApolloTracing.apolloTracing
import caliban.wrappers.Wrappers.{ maxDepth, maxFields, printSlowQueries, timeout }
import zio._
import zio.clock.Clock
import zio.console.Console
import zio.duration._
import zio.stream.ZStream

object Api extends GenericSchema[NotesService] {

  case class Queries(
    @GQLDescription("Return all operations from begining of all times")
    operations: OperationsArgs => RIO[NotesService, List[Operation]]
  )
  case class Mutations(
    addOperation: Operation => RIO[NotesService, Boolean]
  )
  case class Subscriptions(
    addedOperations: OperationsArgs => ZStream[NotesService, Nothing, Operation],
  )

  implicit val operationSchema = gen[Operation]

  val api: GraphQL[Console with Clock with NotesService] =
    graphQL(
      RootResolver(
        Queries(
          args => Service.getOperations(args.noteId)
        ),
        Mutations(
          args => Service.addOperation(args),
        ),
        Subscriptions( 
          args => Service.addedOperations(args.noteId)
       )
      )
    ) @@
      // maxFields(200) @@               // query analyzer that limit query fields
      // maxDepth(30) @@                 // query analyzer that limit query depth
      // timeout(3 seconds) @@           // wrapper that fails slow queries
      printSlowQueries(1 millis) // @@ // wrapper that logs slow queries
      // apolloTracing // wrapper for https://github.com/apollographql/apollo-tracing

}
